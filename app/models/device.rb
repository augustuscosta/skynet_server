class Device < ActiveRecord::Base
  attr_accessible :deviceToken, :empresaToken

  validates :deviceToken, :presence => true, :uniqueness => {:scope => :empresaToken}

	validates_presence_of :deviceToken
	validates_presence_of :empresaToken
end
