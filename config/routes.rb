Vitrine::Application.routes.draw do
  
  resources :devices

  resources :usuarios

  root :to => 'empresas#index'
  
  match 'empresas/conteudo/:identificador' => 'Empresas#get_conteudo_by_identificador'
  
  resources :empresas do
    resources :conteudos
  end
  
  get "empresas/index"

  resources :sessions
  
end
