# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130301185409) do

  create_table "apn_apps", :force => true do |t|
    t.text     "apn_dev_cert"
    t.text     "apn_prod_cert"
    t.text     "apn_empresa_token"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "apn_device_groupings", :force => true do |t|
    t.integer "group_id"
    t.integer "device_id"
  end

  add_index "apn_device_groupings", ["device_id"], :name => "index_apn_device_groupings_on_device_id"
  add_index "apn_device_groupings", ["group_id", "device_id"], :name => "index_apn_device_groupings_on_group_id_and_device_id"
  add_index "apn_device_groupings", ["group_id"], :name => "index_apn_device_groupings_on_group_id"

  create_table "apn_devices", :force => true do |t|
    t.string   "token",              :null => false
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.datetime "last_registered_at"
    t.integer  "app_id"
  end

  add_index "apn_devices", ["token"], :name => "index_apn_devices_on_token"

  create_table "apn_group_notifications", :force => true do |t|
    t.integer  "group_id",          :null => false
    t.string   "device_language"
    t.string   "sound"
    t.string   "alert"
    t.integer  "badge"
    t.text     "custom_properties"
    t.datetime "sent_at"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "apn_group_notifications", ["group_id"], :name => "index_apn_group_notifications_on_group_id"

  create_table "apn_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "app_id"
  end

  create_table "apn_notifications", :force => true do |t|
    t.integer  "device_id",                        :null => false
    t.integer  "errors_nb",         :default => 0
    t.string   "device_language"
    t.string   "sound"
    t.string   "alert"
    t.integer  "badge"
    t.datetime "sent_at"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.text     "custom_properties"
  end

  add_index "apn_notifications", ["device_id"], :name => "index_apn_notifications_on_device_id"

  create_table "apn_pull_notifications", :force => true do |t|
    t.integer  "app_id"
    t.string   "title"
    t.string   "content"
    t.string   "link"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.boolean  "launch_notification"
  end

  create_table "applications", :force => true do |t|
    t.string   "description"
    t.integer  "scenario_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "benchmark_ce3s", :force => true do |t|
    t.string   "name"
    t.integer  "beginHour"
    t.integer  "beginMinute"
    t.integer  "interval"
    t.integer  "rounds"
    t.string   "className"
    t.string   "resultEnd"
    t.integer  "x"
    t.integer  "y"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "components", :force => true do |t|
    t.integer  "virtual_machine_id"
    t.integer  "benchmark_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "conteudos", :force => true do |t|
    t.string   "titulo"
    t.string   "subtitulo"
    t.string   "categoria"
    t.text     "descricao"
    t.string   "endereco"
    t.float    "latitude"
    t.float    "longitude"
    t.boolean  "ativo"
    t.integer  "parent_id"
    t.integer  "empresa_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.boolean  "social"
    t.boolean  "destaque"
    t.string   "url",                     :limit => nil
    t.string   "row_imagem_file_name"
    t.string   "row_imagem_content_type"
    t.integer  "row_imagem_file_size"
    t.datetime "row_imagem_updated_at"
  end

  create_table "crawler_artifacts", :force => true do |t|
    t.integer  "x"
    t.integer  "y"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "devices", :force => true do |t|
    t.string   "deviceToken"
    t.string   "empresaToken"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "empresas", :force => true do |t|
    t.string   "nome"
    t.string   "cnpj"
    t.string   "telefone"
    t.string   "email"
    t.string   "identificador"
    t.string   "token"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.string   "c2dm_email",           :limit => nil
    t.string   "c2dm_password",        :limit => nil
    t.string   "c2dm_registration_id", :limit => nil
    t.string   "facebook"
    t.string   "twitter"
  end

  create_table "key_values", :force => true do |t|
    t.string   "key"
    t.string   "value"
    t.integer  "crawler_artifact_id"
    t.integer  "benchmark_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "metric_evals", :force => true do |t|
    t.boolean  "passed"
    t.decimal  "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "metrics", :force => true do |t|
    t.string   "sla"
    t.string   "name"
    t.integer  "metric_eval_id"
    t.integer  "scenario_id"
    t.integer  "benchmark_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "providers", :force => true do |t|
    t.string   "name"
    t.string   "credentialPath"
    t.string   "userName"
    t.string   "privateKey"
    t.string   "password"
    t.integer  "benchmark_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "scenarios", :force => true do |t|
    t.integer  "benchmark_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "scriptlets", :force => true do |t|
    t.integer  "virtual_machine_startup_id"
    t.integer  "virtual_machine_shutdown_id"
    t.integer  "benchmark_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "sessions", :force => true do |t|
    t.string   "email"
    t.string   "senha"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                               :default => "", :null => false
    t.string   "encrypted_password",   :limit => 128, :default => "", :null => false
    t.string   "password_salt",                       :default => "", :null => false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "reset_password_token"
    t.string   "remember_token"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                       :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "usuarios", :force => true do |t|
    t.string   "nome"
    t.string   "cpf"
    t.string   "rg"
    t.string   "email"
    t.string   "senha"
    t.string   "telefone"
    t.string   "tipo"
    t.integer  "empresa_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "virtual_machine_types", :force => true do |t|
    t.integer  "cpu"
    t.integer  "ram"
    t.string   "providerProfile"
    t.integer  "virtual_machine_id"
    t.integer  "benchmark_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "virtual_machines", :force => true do |t|
    t.string   "name"
    t.string   "publicIpAddress"
    t.string   "privateIpAddress"
    t.integer  "benchmark_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "work_loads", :force => true do |t|
    t.integer  "scenario_id"
    t.integer  "benchmark_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

end
