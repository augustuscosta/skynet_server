class ConteudosController < ApplicationController

  before_filter :get_empresa, :checar_permissoes_empresa_dependente

  require 'apn_on_rails'
  require 'configatron'
  
  def get_empresa
    @empresa = Empresa.find(params[:empresa_id])
  end
  
  # GET /conteudos
  # GET /conteudos.json
  def index
    @conteudos = @empresa.conteudos.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @conteudos }
    end
  end

  # GET /conteudos/1
  # GET /conteudos/1.json
  def show
    @conteudo = @empresa.conteudos.find(params[:id])


    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: [@empresa, @conteudo] }
    end
  end

  # GET /conteudos/new
  # GET /conteudos/new.json
  def new
    @conteudo = Conteudo.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: [@empresa, @conteudo] }
    end
  end

  # GET /conteudos/1/edit
  def edit
    @conteudo = Conteudo.find(params[:id])
  end

  # POST /conteudos
  # POST /conteudos.json
  def create
    @conteudo = @empresa.conteudos.new(params[:conteudo])



    respond_to do |format|
      if @conteudo.save

        @devices = APN::Device.all
        @devices.each do |dev|
          app = APN::App.where(:id => dev.app_id)
          app.each do |ap|
            if (ap.apn_empresa_token == @empresa.token)
              configatron.apn.cert = File.join(RAILS_ROOT, 'config',  @empresa.token + "production.pem") #{}"/config/" + @empresa.token +  ".pem"
              notification = APN::Notification.new
              notification.device = dev
              notification.sound = true
              notification.alert = "Confira as novidades da Cholet!"
              notification.save
              APN::App.send_notifications
            end
          end

          format.html { redirect_to [@empresa, @conteudo], notice: 'Conteudo was successfully created.' }
          format.json { render json: [@empresa, @conteudo], status: :created, location: [@empresa, @conteudo] }

        end

      else
        format.html { render action: "new" }
        format.json { render json: @conteudo.errors, status: :unprocessable_entity }

        logger.info "Processing the request..."
      end
    end
  end

  # PUT /conteudos/1
  # PUT /conteudos/1.json
  def update
    @conteudo = @empresa.conteudos.find(params[:id])

    respond_to do |format|
      if @conteudo.update_attributes(params[:conteudo])
        format.html { redirect_to  [@empresa, @conteudo], notice: 'Conteudo was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @conteudo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conteudos/1
  # DELETE /conteudos/1.json
  def destroy
    @conteudo = Conteudo.find(params[:id])
    @conteudo.destroy

    respond_to do |format|
      format.html { redirect_to [@empresa, @conteudo] }
      format.json { head :no_content }
    end
  end
end
