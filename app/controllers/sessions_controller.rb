class SessionsController < ApplicationController

  skip_before_filter :authorize

  def new
    @session = Session.new
  end

  def create

    if request.post?

        user = Usuario.authenticate(params[:session]['email'], params[:session]['senha'])

        if user

          session[:usuario_id] = user.id


          if user.empresa
            session[:empresa_id] = user.empresa.id
          end

          respond_to do |format|
            format.html {
              if user.tipo == 'administrador'
                redirect_to empresas_path
              else
                if user.empresa
                  @empresa = user.empresa
                  redirect_to(@empresa)
                end
              end
            }
            format.any(:xml, :json) {
              head :ok
            }
          end
          
        else
          respond_to do |format|
            format.html {
              flash[:notice] = "Acesso negado!"
              redirect_to new_session_path
            }
            format.any(:xml, :json) { request_http_basic_authentication 'Web Password' }
          end
          
        end

    end

  end

  def destroy
    
    session[:usuario_id] = nil
    session[:empresa_id] = nil

    respond_to do |format|
      format.html {
        flash[:notice] = "Por Favor, logue no sistema."
        redirect_to new_session_path
      }
    end
  end

end