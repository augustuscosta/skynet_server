class AddC2dmToEmpresa < ActiveRecord::Migration
    def self.up
      add_column :empresas, :c2dm_email,              :String
      add_column :empresas, :c2dm_password,           :String
      add_column :empresas, :c2dm_registration_id,    :String
    end

    def self.down
      remove_column :empresas, :c2dm_email
      remove_column :empresas, :c2dm_password
      remove_column :empresas, :c2dm_registration_id
    end
end
