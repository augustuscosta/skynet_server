class EmpresasController < ApplicationController
  before_filter :checar_permissoes_empresa, :except => :get_conteudo_by_identificador

  require 'apn_on_rails'

  # GET /empresas
  # GET /empresas.json
  def index
    @empresas = Empresa.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @empresas }
    end
  end

  # GET /empresas/1
  # GET /empresas/1.json
  def show
    @empresa = Empresa.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @empresa }
    end
  end

  # GET /empresas/new
  # GET /empresas/new.json
  def new
    @empresa = Empresa.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @empresa }
    end
  end

  # GET /empresas/1/edit
  def edit
    @empresa = Empresa.find(params[:id])
  end

  # POST /empresas
  # POST /empresas.json
  def create
    @empresa = Empresa.new(params[:empresa])

      if(!@empresa.token.empty?)
        if (!APN::App.where(:apn_empresa_token => @empresa.token).first)
          APN::App.create(:apn_empresa_token => @empresa.identificador, :apn_dev_cert =>  File.read('config/' + @empresa.token + "development.pem"), :apn_prod_cert => File.read('config/' + @empresa.token + "production.pem"))
        end
      end

    respond_to do |format|
      if @empresa.save
        format.html { redirect_to @empresa, notice: 'Empresa was successfully created.' }
        format.json { render json: @empresa, status: :created, location: @empresa }
      else
        format.html { render action: "new" }
        format.json { render json: @empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /empresas/1
  # PUT /empresas/1.json
  def update
    @empresa = Empresa.find(params[:id])

    if(!@empresa.token.empty?)
      if (!APN::App.where(:apn_empresa_token => @empresa.token).first)
        APN::App.create(:apn_empresa_token => @empresa.identificador, :apn_dev_cert =>  File.read('config/' + @empresa.token + "development.pem"), :apn_prod_cert => File.read('config/' + @empresa.token + "production.pem"))
      end
    end

    respond_to do |format|
      if @empresa.update_attributes(params[:empresa])
        format.html { redirect_to @empresa, notice: 'Empresa was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /empresas/1
  # DELETE /empresas/1.json
  def destroy
    @empresa = Empresa.find(params[:id])
    @empresa.destroy

    respond_to do |format|
      format.html { redirect_to empresas_url }
      format.json { head :no_content }
    end
  end
  
  def get_conteudo_by_identificador
    @empresa = Empresa.where("identificador = ?", params[:identificador])
    
    @conteudos = Conteudo.where("empresa_id = ?", @empresa)
    
    respond_to do |format|
      format.json { render :json => @conteudos }
    end
  end
end
