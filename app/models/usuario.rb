class Usuario < ActiveRecord::Base
	belongs_to :empresa 
	validates_uniqueness_of :email
	validates_presence_of 	:email
	validates_uniqueness_of :cpf
	validates_presence_of 	:cpf

	def self.authenticate(email, senha)
    user = self.find_by_email(email)
    if user
      if user.senha != senha
        user = nil
      end
    end
    user
  end

end
