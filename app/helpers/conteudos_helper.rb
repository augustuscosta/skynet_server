module ConteudosHelper

	#concat('</br>'.html_safe)

	 def display_conteudos(conteudos)
       content_tag(:div, :class => 'container') do
       	conteudos.collect do |conteudo|
           	if conteudo.parent_id == nil
           		concat(
            		content_tag(:div, :class => 'conteudo', :id => 'accordion') do
            			 concat(
                        content_tag(:ul, :class => 'show') do link_to(image_tag("/assets/shared/show.png", :border=>0, :alt=> 'Mostrar Conteudo', :title => 'Mostrar Conteudo'), [@empresa,conteudo] ) end
                      )
                   concat(
                        content_tag(:ul, :class => 'editar') do link_to(image_tag("/assets/shared/editar.png", :border=>0, :alt=> 'Editar', :title => 'Editar'), edit_empresa_conteudo_path(@empresa,conteudo)) end
                      )
                  concat(
                        content_tag(:ul, :class => 'excluir') do link_to(image_tag("/assets/shared/remover.png", :border=>0, :alt=> 'Remover', :title => 'Remover'), [@empresa,conteudo], confirm: 'Tem certeza?', method: :delete) end
                      )
                  concact_conteudo(conteudo)
            			concat(find_all_subconteudos(conteudo))
                 
            		end
            	)
           	end
         	end
       end
    end
    
   def find_all_subconteudos(conteudo)
    if conteudo.children.size > 0
        content_tag(:div, :class => 'sub_conteudo') do
      	conteudo.children.collect do |subcat|
        	if subcat.children.size > 0
        		 
            concat(
          			content_tag(:div,:class => 'conteudo') do
                  concat(
                        content_tag(:ul, :class => 'show') do link_to(image_tag("/assets/shared/show.png", :border=>0, :alt=> 'Mostrar Conteudo', :title => 'Mostrar Conteudo'), [@empresa,subcat] ) end
                      )
          				concat(
                        content_tag(:ul, :class => 'editar') do link_to(image_tag("/assets/shared/editar.png", :border=>0, :alt=> 'Editar', :title => 'Editar'), edit_empresa_conteudo_path(@empresa,subcat)) end
                      )
                  concat(
                        content_tag(:ul, :class => 'excluir') do link_to(image_tag("/assets/shared/remover.png", :border=>0, :alt=> 'Remover', :title => 'Remover'), [@empresa,subcat], confirm: 'Tem certeza?', method: :delete) end
                      )
                  concact_conteudo(subcat)
          				concat(find_all_subconteudos(subcat))
          			end
          		)
        	else
        		concat(
          			content_tag(:div,:class => 'conteudo') do
                  concat(
                        content_tag(:ul, :class => 'show') do link_to(image_tag("/assets/shared/show.png", :border=>0, :alt=> 'Mostrar Conteudo', :title => 'Mostrar Conteudo'), [@empresa,subcat] ) end
                      )
          				concat(
                        content_tag(:ul, :class => 'editar') do link_to(image_tag("/assets/shared/editar.png", :border=>0, :alt=> 'Editar', :title => 'Editar'), edit_empresa_conteudo_path(@empresa,subcat)) end
                      )
                  concat(
                        content_tag(:ul, :class => 'excluir') do link_to(image_tag("/assets/shared/remover.png", :border=>0, :alt=> 'Remover', :title => 'Remover'), [@empresa,subcat], confirm: 'Tem certeza?', method: :delete) end
                      )
                  concact_conteudo(subcat)
          			end
          		)
        	end
        end
      end
    end
  end

  def concact_conteudo(conteudo)
  	concat(
      content_tag(:h3, conteudo.titulo, :class => 'label_header') do 

        concat(
          content_tag(:div) do

          concat(
            content_tag(:div) do image_tag(conteudo.imagem(:thumb), :class => 'image_conteudo_thumb') end
          )
          concat(
            content_tag(:div, conteudo.titulo, :class => 'label_titulo')
          )
          concat(
            content_tag(:div, conteudo.subtitulo, :class => 'label_subtitulo')
          )
          concat(
            content_tag(:div, conteudo.categoria, :class => 'label_categoria')
          )
          concat(
            content_tag(:div, conteudo.descricao.html_safe, :class => 'label_descricao')
          )
          
          
         end
         )
        
                
      end
      )
      
    content_tag(:div) do

          concat(
            content_tag(:li) do link_to('Editar', edit_empresa_conteudo_path(@empresa,conteudo)) end
          )

          concat(
            content_tag(:li) do link_to('Excluir', [@empresa,conteudo], confirm: 'Tem certeza?', method: :delete) end
          )
         
          concat(
            content_tag(:li) do image_tag(conteudo.imagem(:thumb), :class => 'image_conteudo_thumb') end
          )
        
          concat(
            content_tag(:li, conteudo.subtitulo, :class => 'label_subtitulo')
          )
         concat(
            content_tag(:li, conteudo.categoria, :class => 'label_categoria')
          )
         concat(
            content_tag(:li, conteudo.descricao, :class => 'label_descricao')
          )

         end
         
         
           
       
    
  end
  
end
