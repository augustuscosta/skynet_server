class AddDestaqueAndSocialToConteudo < ActiveRecord::Migration
  def self.up
      add_column :conteudos, :social,    :boolean
      add_column :conteudos, :destaque,    :boolean
    end

    def self.down
      remove_column :conteudos, :social
      remove_column :conteudos, :destaque
    end
end
