class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :authorize, :except => :get_conteudo_by_identificador

  def authorize

        if session[:usuario_id].nil?
        respond_to do |format|
          format.html do
            flash[:notice] = "Por Favor, logue no sistema."
            redirect_to new_session_path
          end
          format.any(:xml, :json) { head :fail }
        end
      end

  end


  def check_role(empresa)

    if session[:usuario_id]
      user = Usuario.find(session[:usuario_id])
      if user.tipo != 'administrador'
        if user.empresa.id != empresa.id
          render :file => "#{Rails.public_path}/404.html", :status => :unauthorized
        end
      end
    end

  end

  def checar_permissoes_empresa
    if params[:id]
      empresa = Empresa.find(params[:id])
      if empresa
        check_role(empresa)
      end
    else
      user = Usuario.find(session[:usuario_id])
      if user.tipo != 'administrador'
        render :file => "#{Rails.public_path}/404.html", :status => :unauthorized
      end
    end
  end

  def checar_permissoes_usuario
    user = Usuario.find(session[:usuario_id])
    if user.tipo != 'administrador'
      render :file => "#{Rails.public_path}/404.html", :status => :unauthorized
    end
  end

  def checar_permissoes_empresa_dependente
    if params[:empresa_id]
      empresa = Empresa.find(params[:empresa_id])
      if empresa
        check_role(empresa)
      end
    end
  end

end
