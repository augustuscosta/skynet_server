class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string  :nome
      t.string  :cpf
      t.string  :rg
      t.string  :email
      t.string  :senha
      t.string  :telefone
      t.string  :tipo
      t.integer :empresa_id

      t.timestamps
    end

    Usuario.create :nome => "administrador", :cpf => "00000000", :tipo => "administrador", :email => "administrador@otgmobile.com.br", :senha => "1q2w3e"
  end
end
