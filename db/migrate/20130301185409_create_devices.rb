class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :deviceToken
      t.string :empresaToken

      t.timestamps
    end
  end
end
