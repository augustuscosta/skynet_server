class CreateSessionsTable < ActiveRecord::Migration
  def up
  	create_table :sessions do |t|
  		t.string :email
  		t.string :senha

  		t.timestamps
	end
  end

  def down
  end
end
